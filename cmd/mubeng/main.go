package main

import (
	"github.com/projectdiscovery/gologger"
	"gitlab.com/vay3t/mubeng/internal/runner"
)

func main() {
	opt := runner.Options()

	if err := runner.New(opt); err != nil {
		gologger.Fatal().Msgf("Error! %s", err)
	}
}
