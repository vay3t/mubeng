package server

import (
	"github.com/elazarl/goproxy"
	"gitlab.com/vay3t/mubeng/common"
)

// Proxy as ServeMux in proxy server handler.
type Proxy struct {
	HTTPProxy *goproxy.ProxyHttpServer
	Options   *common.Options
}
